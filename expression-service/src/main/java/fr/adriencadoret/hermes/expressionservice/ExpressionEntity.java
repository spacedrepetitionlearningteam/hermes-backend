package fr.adriencadoret.hermes.expressionservice;

public class ExpressionEntity {

    private String sourceLanguage;
    private String sourceText;
    private String destinationLanguage;
    private String destinationText;

    public ExpressionEntity(String sourceLanguage, String sourceText, String destinationLanguage, String destinationText) {
        this.sourceLanguage = sourceLanguage;
        this.sourceText = sourceText;
        this.destinationLanguage = destinationLanguage;
        this.destinationText = destinationText;
    }

    public String getSourceLanguage() {
        return sourceLanguage;
    }

    public void setSourceLanguage(String sourceLanguage) {
        this.sourceLanguage = sourceLanguage;
    }

    public String getSourceText() {
        return sourceText;
    }

    public void setSourceText(String sourceText) {
        this.sourceText = sourceText;
    }

    public String getDestinationLanguage() {
        return destinationLanguage;
    }

    public void setDestinationLanguage(String destinationLanguage) {
        this.destinationLanguage = destinationLanguage;
    }

    public String getDestinationText() {
        return destinationText;
    }

    public void setDestinationText(String destinationText) {
        this.destinationText = destinationText;
    }

    @Override
    public String toString() {
        return "ExpressionEntity{" +
                "sourceLanguage='" + sourceLanguage + '\'' +
                ", sourceText='" + sourceText + '\'' +
                ", destinationLanguage='" + destinationLanguage + '\'' +
                ", destinationText='" + destinationText + '\'' +
                '}';
    }
}
