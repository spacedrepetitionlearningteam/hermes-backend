package fr.adriencadoret.hermes.expressionservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableDiscoveryClient
public class ExpressionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExpressionServiceApplication.class, args);
	}


    @RestController
    @RefreshScope
	class ExpressionServiceRestController {

        @Value("${message:Hello Everyone!}")
        private String configMessage;

        @GetMapping("/all")
        public List<ExpressionEntity> getAllExpressions(){
            List<ExpressionEntity> expressionEntities = new ArrayList<>();
            expressionEntities.add(new ExpressionEntity("FR", "Bonjour", "EN", "Hello"));
            return expressionEntities;
        }

        @GetMapping("/config-message")
        public String getConfigMessage(){
            return configMessage;
        }
    }
}
