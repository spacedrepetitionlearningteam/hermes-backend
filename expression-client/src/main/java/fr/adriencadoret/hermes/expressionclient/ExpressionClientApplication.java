package fr.adriencadoret.hermes.expressionclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
public class ExpressionClientApplication {


	@Bean @LoadBalanced
	RestTemplate restTemplate(){
		return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(ExpressionClientApplication.class, args);
	}

	@RestController
	class ExpressionApiGatewayRestController{

		private final RestTemplate restTemplate;

		@Autowired
		public ExpressionApiGatewayRestController(RestTemplate restTemplate) {
			this.restTemplate = restTemplate;
		}

		@GetMapping("/message")
		public String getMessage(){
			return this.restTemplate.getForObject("http://expression-service/config-message", String.class);
		}


	}
}
